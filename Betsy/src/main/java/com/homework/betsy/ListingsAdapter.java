package com.homework.betsy;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.homework.betsy.model.Listing;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class ListingsAdapter extends ArrayAdapter<Listing> {
    private static final String LOG_TAG = "ListingsAdapter";

    public ListingsAdapter(Context context, int resource) {
        super(context, resource, new ArrayList<Listing>());
    }

    @Override
    public View getView(final int position, View recycledView, ViewGroup parent) {
        ListingsViewHolder viewHolder;
        Listing listing = getItem(position);

        if (recycledView == null) {
            recycledView = LayoutInflater.from(getContext()).inflate(R.layout.listing, parent, false);

            viewHolder = new ListingsViewHolder();
            viewHolder.imageImageView = (ImageView) recycledView.findViewById(R.id.listing_image);
            viewHolder.titleTextView = (TextView) recycledView.findViewById(R.id.listing_title);
            viewHolder.position = position;

            recycledView.setTag(viewHolder);
        } else {
            viewHolder = (ListingsViewHolder) recycledView.getTag();
        }

        if (viewHolder.position != position) {
            // Show the background while we load the new image for this listing.
            viewHolder.imageImageView.setImageResource(android.R.color.transparent);

            if (viewHolder.imageTaskRef != null) {
                DownloadImageTask previousImageTask = viewHolder.imageTaskRef.get();
                if (previousImageTask != null && !previousImageTask.isCancelled()) {
                    // Cancel the previous DownloadImageTask as the user has scrolled through the
                    // list faster than the images have loaded.
                    previousImageTask.cancel(true);
                }
            }
        }
        viewHolder.position = position;

        if (listing != null) {
            viewHolder.titleTextView.setText(listing.getTitle());

            if (listing.getImageData() != null) {
                DownloadImageTask downloadImageTask = new DownloadImageTask(
                        BetsyActivity.getLruCache(), listing.getImageData().getUrl570xN(), position);
                viewHolder.imageTaskRef = new WeakReference<DownloadImageTask>(downloadImageTask);
                downloadImageTask.execute(viewHolder);
            }
        }

        return recycledView;
    }

    // Classes

    public static class ListingsViewHolder extends ViewHolder {
        TextView titleTextView;
    }
}
