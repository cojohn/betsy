package com.homework.betsy;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.*;
import android.widget.*;
import com.homework.betsy.api.ApiRequest;
import com.homework.betsy.api.ApiResponse;
import com.homework.betsy.api.ApiTask;
import com.homework.betsy.api.ListingsResponse;
import com.homework.betsy.model.Listing;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Fragment that will display Listings returned from the Etsy API.
 */
public class ListingsFragment extends Fragment implements AdapterView.OnItemClickListener {
    private static final String LOG_TAG             = "ListingsFragment";

    /**
     * Bundle keys for saving state during fragment destruction and creation; e.g. on rotation.
     */
    private static final String BUNDLE_KEY_COUNT    = "count";
    private static final String BUNDLE_KEY_FILTER   = "filter";
    private static final String BUNDLE_KEY_KEYWORD  = "keyword";
    private static final String BUNDLE_KEY_LISTINGS = "listings";
    private static final String BUNDLE_KEY_OFFSET   = "offset";

    /**
     * Maximum number of results to return in a single API request.
     */
    private static final int QUERY_LIMIT            = 25;

    /**
     * Data adapter for the ListView that will display Listings.
     */
    private ListingsAdapter mAdapter;

    /**
     * The last search keyword used; stored to support infinite scroll (follow-up queries) on the
     * keyword.
     */
    private String mKeyword;

    /**
     * The number of repeated queries on the the current keyword.
     */
    private int mOffset;

    /**
     * The total number of results we can expect from the last query.
     */
    private long mCount;

    /**
     * A flag to see if we have an infinite scroll request already in-process before we attempt
     * another one.
     */
    private boolean mQuerying;

    /**
     * An additional tag filter for the current keyword.
     */
    private String mFilter;

    /**
     * A listener for the Activity
     */
    private OnListingSelectedListener mOnListingSelectedListener;

    public ListingsFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAdapter = new ListingsAdapter(getActivity(), R.layout.listing);

        if (savedInstanceState != null) {
            mCount = savedInstanceState.getLong(BUNDLE_KEY_COUNT);
            mFilter = savedInstanceState.getString(BUNDLE_KEY_FILTER);
            mKeyword = savedInstanceState.getString(BUNDLE_KEY_KEYWORD);
            ArrayList<Listing> listings = savedInstanceState.getParcelableArrayList(BUNDLE_KEY_LISTINGS);
            mAdapter.addAll(listings);
            mOffset = savedInstanceState.getInt(BUNDLE_KEY_OFFSET);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listings, container, false);

        if (view != null) {
            ListView listView = (ListView) view.findViewById(R.id.listings_list_view);
            if (listView != null) {
                listView.setAdapter(mAdapter);
                listView.setLayoutAnimation(AnimationUtils.loadLayoutAnimation(getActivity(), R.anim.listings_in));
                listView.setEmptyView(view.findViewById(R.id.listings_empty));
                listView.setOnItemClickListener(this);

                final WeakReference<ListingsFragment> weakSelf = new WeakReference<ListingsFragment>(this);
                listView.setOnScrollListener(new AbsListView.OnScrollListener() {
                    @Override
                    public void onScrollStateChanged(AbsListView view, int scrollState) {

                    }

                    @Override
                    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                        if (firstVisibleItem + visibleItemCount >= totalItemCount) {
                            // The user has scrolled the ListView and drawn the last available
                            // Listing (it may not be visible yet). Fires multiple times, so check
                            // if a query is already in progress. We also check the count against
                            // the total number of items in the list, so that we don't infinite
                            // query at the end of a result set.
                            ListingsFragment self = weakSelf.get();
                            if (self != null && !self.mQuerying && self.mCount > totalItemCount) {
                                self.search(self.mKeyword, self.mFilter);
                            }
                        }
                    }
                });
            }
        }

        return view;
    }

    /**
     * Save the current state of the Fragment before its destruction. It's particularly important
     * that we save the ArrayList of previously fetched API results to prevent new network calls
     * at the start of the new lifecycle.
     *
     * @param outState The Bundle representing the Fragment's current state; passed to onCreate().
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (outState == null) {
            outState = new Bundle();
        }

        // A getItems() function on the data adapter would be a better way to build and cache, or
        // just reference, the ArrayList of all Listings. Extend BaseAdapter instead of
        // ArrayAdapter, maybe?
        ArrayList<Listing> listings = new ArrayList<Listing>();
        for (int i = 0; i < mAdapter.getCount(); i++) {
            listings.add(mAdapter.getItem(i));
        }

        outState.putLong(BUNDLE_KEY_COUNT, mCount);
        outState.putString(BUNDLE_KEY_FILTER, mFilter);
        outState.putString(BUNDLE_KEY_KEYWORD, mKeyword);
        outState.putParcelableArrayList(BUNDLE_KEY_LISTINGS, listings);
        outState.putInt(BUNDLE_KEY_OFFSET, mOffset);
    }

    @Override
    public void onStart() {
        super.onStart();

        if (mKeyword == null) {
            // Why not?
            search("hat", "");
        }
    }

    // Implementation for AdapterView.OnItemClickListener

    /**
     * Fired when the user selects a Listing in this Fragment's ListView. Alert any attached
     * listeners to the event.
     *
     * @param parent The ListView triggered by the click event.
     * @param view The Listing (View) clicked.
     * @param position The View's position in the adapter.
     * @param id The row identifier (should be the same as position for this).
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Listing listing = mAdapter.getItem(position);
        if (mOnListingSelectedListener != null) {
            mOnListingSelectedListener.listingSelected(listing);
        }
    }

    // Custom methods

    public void attachListener(OnListingSelectedListener listener) {
        mOnListingSelectedListener = listener;
    }

    /**
     * Detach the listener from the Fragment. The listener is passed as a parameter in the event
     * the class is extended to support multiple listeners.
     *
     * @param listener The OnListingSelectedListener to detach.
     */
    public void detachListener(OnListingSelectedListener listener) {
        mOnListingSelectedListener = null;
    }

    /**
     * Called when the user attempts a search without an active network connection.
     */
    private void handleNotConnected() {
        if (mAdapter.getCount() > 0) {
            // The user has already loaded some listings.
            Toast.makeText(getActivity(), getString(R.string.listings_no_network), Toast.LENGTH_SHORT).show();
            return;
        }

        View view = getView();
        if (view != null) {
            ViewGroup emptyGroup = (ViewGroup) view.findViewById(R.id.listings_empty);
            if (emptyGroup != null) {
                TextView keyword = (TextView) emptyGroup.findViewById(R.id.listings_empty_keyword);
                if (keyword != null) {
                    keyword.setText(getString(R.string.listings_empty_no_network));
                }
            }
        }
    }

    /**
     * Interprets a ListingsResponse returned by the Etsy API and updates the Fragment state.
     */
    private void handleResults(ListingsResponse response) {
        // Add all of the found listings into the data adapter and refresh.
        if (response.getListings().size() > 0) {
            mAdapter.addAll(response.getListings());
            mAdapter.notifyDataSetChanged();
        }
        mCount = response.getCount();

        View view = getView();
        if (view != null) {
            // There are no results for this search keyword
            ViewGroup emptyGroup = (ViewGroup) view.findViewById(R.id.listings_empty);
            if (emptyGroup != null) {
                TextView keyword = (TextView) emptyGroup.findViewById(R.id.listings_empty_keyword);
                if (keyword != null) {
                    if (response.getCount() == 0) {
                        // Set the zero listings message.
                        keyword.setText(String.format(getString(R.string.listings_zero_listings), mKeyword));
                    } else {
                        // Set to empty so that the network or zero listings message is removed.
                        keyword.setText("");
                    }
                }
            }

            final WeakReference<ListingsFragment> weakSelf = new WeakReference<ListingsFragment>(this);
            ViewGroup viewGroup = (ViewGroup) view.findViewById(R.id.listings_tags_container);
            viewGroup.removeAllViews();
            // Add some (8-10) tags for the user to pivot on.
            for (int i = 0; i < 10; i++) {
                if (response.getTags().size() > 0) {
                    final String tag = response.getTags().pop();
                    if (tag.equals(mKeyword) || tag.equals(mFilter)) {
                        // Don't suggest "hat" if searching for "hat".
                        continue;
                    }

                    TextView t = (TextView) LayoutInflater.from(getActivity()).inflate(R.layout.tag, null);
                    t.setText(tag);

                    t.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ListingsFragment self = weakSelf.get();
                            if (self != null) {
                                self.search(self.mKeyword, tag);
                            }
                        }
                    });
                    viewGroup.addView(t);
                }
            }

            final View searching = view.findViewById(R.id.listings_searching);

            AnimationSet hide = new AnimationSet(true);
            hide.setDuration(400);
            hide.setInterpolator(new AccelerateInterpolator());

            AlphaAnimation alpha = new AlphaAnimation(1f, 0f);
            hide.addAnimation(alpha);

            TranslateAnimation translate = new TranslateAnimation(0f, 0f, 0f, searching.getHeight());
            hide.addAnimation(translate);
            hide.setAnimationListener(new Animation.AnimationListener() {
                @Override
                public void onAnimationStart(Animation animation) {

                }

                @Override
                public void onAnimationEnd(Animation animation) {
                    searching.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationRepeat(Animation animation) {

                }
            });
            searching.clearAnimation();
            searching.startAnimation(hide);
        }
    }

    /**
     * Search the Etsy API for Listing results matching a user-supplied keyword. On a successful
     * response, refresh the ListView with found Listings.
     *
     * @param keyword A String passed to the API to filter Listing results.
     */
    public void search(String keyword, String filter) {
        if (mQuerying) {
            // Don't perform multiple queries at the same time; maybe queue them up?
            return;
        }
        mQuerying = true;

        if (keyword.equals(mKeyword) && filter.equals(mFilter)) {
            mOffset += QUERY_LIMIT;
        } else {
            // Clear the current listings.
            mAdapter.clear();
            mAdapter.notifyDataSetChanged();
            mOffset = 0;
        }
        mFilter = filter;
        mKeyword = keyword;

        ConnectivityManager cm = (ConnectivityManager) getActivity()
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        boolean isConnected = cm != null && cm.getActiveNetworkInfo() != null &&
                cm.getActiveNetworkInfo().isConnected();

        if (!isConnected) {
            handleNotConnected();
            mQuerying = false;
            return;
        }

        View view = getView();
        if (view != null) {
            view.findViewById(R.id.listings_searching_image)
                    .startAnimation(AnimationUtils.loadAnimation(getActivity(), R.anim.rotate_forever));
            View searching = view.findViewById(R.id.listings_searching);

            AnimationSet show = new AnimationSet(true);
            show.setDuration(400);
            show.setInterpolator(new AccelerateInterpolator());

            AlphaAnimation alpha = new AlphaAnimation(0f, 1f);
            show.addAnimation(alpha);

            TranslateAnimation translate = new TranslateAnimation(0f, 0f, searching.getHeight(), 0f);
            show.addAnimation(translate);

            searching.setVisibility(View.VISIBLE);
            searching.clearAnimation();
            searching.startAnimation(show);
        }

        ApiRequest apiRequest = new ApiRequest.Builder()
                .setKeyword(mFilter == null ? mKeyword : String.format("%s %s", mKeyword, mFilter))
                .setOffset(mOffset)
                .build();

        final WeakReference<ListingsFragment> weakSelf = new WeakReference<ListingsFragment>(this);
        ApiTask listingsSearch = new ApiTask();
        listingsSearch.setResponseListener(new ApiTask.ResponseListener() {
            @Override
            public void onApiResponse(ApiResponse response) {
                ListingsFragment self = weakSelf.get();
                if (self != null) {
                    if (response instanceof ListingsResponse) {
                        self.handleResults((ListingsResponse) response);
                    }
                    self.mQuerying = false;
                }
            }

            /**
             * Handle errors raised either by attempts to contact the Etsy API or returned by the
             * Etsy API. Let the user know there was a problem finding listings and (potentially)
             * encourage the user to try again.
             *
             * @param errorCode Error code (reason) as defined in ApiTask.
             * @param error An Error object returned by the API; may be null.
             */
            @Override
            public void onApiError(ApiTask.ErrorCode errorCode, ApiResponse error) {
                ListingsFragment self = weakSelf.get();
                if (self != null && self.getActivity() != null) {
                    self.mQuerying = false;
                    self.mCount = 0;
                    Toast.makeText(self.getActivity(), "Error fetching listings!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        listingsSearch.execute(apiRequest);
    }

    public interface OnListingSelectedListener {
        public void listingSelected(Listing listing);
    }
}

