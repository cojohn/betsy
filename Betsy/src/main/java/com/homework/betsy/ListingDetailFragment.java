package com.homework.betsy;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.homework.betsy.model.Listing;

import java.lang.ref.WeakReference;

/**
 * Provides a detailed view of a Listing.
 */
public class ListingDetailFragment extends Fragment {
    /**
     * Bundles keys for state management.
     */
    protected static final String BUNDLE_KEY_LISTING = "listing";

    private Listing mListing;

    public ListingDetailFragment() {
        super();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mListing = savedInstanceState.getParcelable(BUNDLE_KEY_LISTING);
        } else if (getArguments() != null) {
            mListing = getArguments().getParcelable(BUNDLE_KEY_LISTING);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_listing_detail, container, false);

        if (view != null) {
            ImageView imageView = (ImageView) view.findViewById(R.id.listing_detail_image);
            if (imageView != null) {
                ViewHolder viewHolder = new ViewHolder();
                viewHolder.imageImageView = imageView;
                viewHolder.position = 0;
                imageView.setTag(viewHolder);
                DownloadImageTask downloadImageTask = new DownloadImageTask(
                        BetsyActivity.getLruCache(), mListing.getImageData().getUrl570xN(), 0);
                viewHolder.imageTaskRef = new WeakReference<DownloadImageTask>(downloadImageTask);
                downloadImageTask.execute(viewHolder);
            }

            TextView titleTextView = (TextView) view.findViewById(R.id.listing_detail_title);
            if (titleTextView != null) {
                titleTextView.setText(mListing.getTitle());
            }

            TextView descTextView = (TextView) view.findViewById(R.id.listing_detail_description);
            if (descTextView != null) {
                descTextView.setText(mListing.getDescription());
            }
        }

        return view;
    }

    /**
     * Save the listing as the Fragment shuts down in case its details have changed since it was
     * attached to arguments.
     *
     * @param outState A Bundle representing the state of the Fragment.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        if (outState == null) {
            outState = new Bundle();
        }

        outState.putParcelable(BUNDLE_KEY_LISTING, mListing);
    }
}
