package com.homework.betsy;

import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class ViewHolder {
    ImageView imageImageView;
    WeakReference<DownloadImageTask> imageTaskRef;
    int position;
}
