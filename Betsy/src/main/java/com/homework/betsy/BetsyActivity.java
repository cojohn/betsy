package com.homework.betsy;

import android.graphics.Bitmap;
import android.support.v4.app.FragmentManager;
import android.support.v4.util.LruCache;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import com.homework.betsy.api.ApiRequest;
import com.homework.betsy.model.Listing;

import java.lang.ref.WeakReference;


public class BetsyActivity extends ActionBarActivity
        implements ListingsFragment.OnListingSelectedListener,
        FragmentManager.OnBackStackChangedListener {
    private static final String LOG_TAG                     = "BetsyActivity";
    private static final String FRAGMENT_TAG_LISTING_DETAIL = "ListingDetailFragment";
    private static final String FRAGMENT_TAG_LISTINGS       = "ListingsFragment";

    /**
     * Hold Bitmaps in memory temporarily while the user browses listings and listing details.
     */
    private static LruCache<String, Bitmap> mLruCache;

    // Implementation for ActionBarActivity

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_betsy);

        ApiRequest.setApiKey(getString(R.string.etsy_api_key));

        if (mLruCache == null) {
            // Override sizeOf to return the byte count of the stored Bitmap; otherwise the cache
            // would store 2^23 Bitmaps. Better as a function of Runtime memory with a non-static
            // cache? The 570xN images are generally between 1MB and 2MB so this cache is small.
            mLruCache = new LruCache<String, Bitmap>(1024 * 1024 * 8) {
                protected int sizeOf(String key, Bitmap value) {
                    return value.getByteCount();
                }
            };
        }

        getSupportFragmentManager().addOnBackStackChangedListener(this);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new ListingsFragment(), FRAGMENT_TAG_LISTINGS)
                    .commit();
        } else if (getSupportFragmentManager().getBackStackEntryCount() > 0 &&
                getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.betsy, menu);
        final WeakReference<BetsyActivity> weakSelf = new WeakReference<BetsyActivity>(this);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                Log.d(LOG_TAG, String.format("search: %s", s));
                BetsyActivity self = weakSelf.get();
                if (self != null) {
                    self.searchByKeyword(s);
                }
                return false;
            }

              @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return true;
    }

    /**
     * Handle user interaction with the action bar.
     *
     * @param item The item clicked on the action bar.
     * @return True if handled here; otherwise the return value of the super method.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        } else if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();

        ListingsFragment listings = (ListingsFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAGMENT_TAG_LISTINGS);
        if (listings != null) {
            listings.attachListener(this);
        }
    }

    @Override
    public void onStop() {
        ListingsFragment listings = (ListingsFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAGMENT_TAG_LISTINGS);
        if (listings != null) {
            listings.detachListener(this);
        }

        super.onStop();
    }

    // Implementation for ListingsFragment.OnListingSelectedListener

    @Override
    public void listingSelected(Listing listing) {
        ListingDetailFragment listingDetailFragment = new ListingDetailFragment();

        Bundle args = new Bundle();
        args.putParcelable(ListingDetailFragment.BUNDLE_KEY_LISTING, listing);
        listingDetailFragment.setArguments(args);

        getSupportFragmentManager()
                .beginTransaction()
                // setCustomAnimations must be called before add/replace.
                .setCustomAnimations(R.anim.slide_in_from_right, R.anim.slide_out_to_right,
                        R.anim.slide_in_from_right, R.anim.slide_out_to_right)
                .add(R.id.container, listingDetailFragment, FRAGMENT_TAG_LISTING_DETAIL)
                .addToBackStack(FRAGMENT_TAG_LISTING_DETAIL)
                .commit();

        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
    }

    // Implementation for FragmentManager.OnBackStackChangedListener

    /**
     * Fired when the FragmentManager's stack is changed.
     */
    @Override
    public void onBackStackChanged() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 0 &&
                getSupportActionBar() != null) {
            // Hide the up navigation in the action bar if we've got nothing on the fragment stack.
            getSupportActionBar().setHomeButtonEnabled(false);
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            getSupportActionBar().setDisplayShowHomeEnabled(false);
        }
    }

    // Custom methods

    protected static LruCache<String, Bitmap> getLruCache() {
        return mLruCache;
    }

    private void searchByKeyword(String keyword) {
        ListingsFragment listingsFragment = (ListingsFragment) getSupportFragmentManager()
                .findFragmentByTag(FRAGMENT_TAG_LISTINGS);

        if (listingsFragment != null) {
            listingsFragment.search(keyword, "");
        }

        while (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            getSupportFragmentManager().popBackStackImmediate();
        }
    }
}
