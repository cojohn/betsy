package com.homework.betsy;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.view.animation.AlphaAnimation;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Retrieves a Bitmap from a URL for display in an ImageView.
 */
public class DownloadImageTask extends AsyncTask<ViewHolder, Void, Bitmap> {
    private static final String LOG_TAG = "DownloadImageTask";

    private final LruCache<String, Bitmap> mCache;
    private final String mUrl;
    private boolean mAnimate;
    private ViewHolder mViewHolder;
    private final int mPosition;

    public DownloadImageTask(LruCache<String, Bitmap> cache, String url, int position) {
        mCache = cache;
        mPosition = position;
        mUrl = url;
    }

    @Override
    protected Bitmap doInBackground(ViewHolder... viewHolders) {
        mViewHolder = viewHolders[0];
        Bitmap bitmap;

        synchronized (mCache) {
            bitmap = mCache.get(mUrl);
        }

        if (bitmap == null) {
            Log.v(LOG_TAG, String.format("Cache miss: %s", mUrl));
            mAnimate = true;
            try {
                bitmap = BitmapFactory.decodeStream(new URL(mUrl).openStream());
            } catch (MalformedURLException unexpected) {
                Log.e(LOG_TAG, "Bogus url", unexpected);
            } catch (IOException unexpected) {
                Log.e(LOG_TAG, "I/O error", unexpected);
            } catch (OutOfMemoryError lotsOfImages) {
                Log.e(LOG_TAG, "Clearing the image cache after an OOM", lotsOfImages);
                synchronized (mCache) {
                    mCache.evictAll();
                }
            }
        } else {
            Log.v(LOG_TAG, String.format("Cache hit: %s", mUrl));
        }

        if (bitmap != null) {
            synchronized (mCache) {
                if (mCache.get(mUrl) == null) {
                    mCache.put(mUrl, bitmap);
                }
            }
        }

        return bitmap;
    }

    @Override
    public void onPostExecute(Bitmap bitmap) {
        if (mViewHolder == null || bitmap == null) {
            return;
        }

        if (mViewHolder.position == mPosition) {
            mViewHolder.imageTaskRef = null;
            ImageView imageView = mViewHolder.imageImageView;
            imageView.setImageBitmap(bitmap);
            if (mAnimate) {
                AlphaAnimation alphaAnimation = new AlphaAnimation(0.0f, 1.0f);
                alphaAnimation.setDuration(1000);
                alphaAnimation.setInterpolator(new DecelerateInterpolator());
                imageView.startAnimation(alphaAnimation);
            }
        }
    }
}
