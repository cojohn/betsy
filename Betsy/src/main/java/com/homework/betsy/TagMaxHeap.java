package com.homework.betsy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * A MaxHeap for sorting tags (well, any String right now) by frequency.
 */
public class TagMaxHeap {
    private static class Item implements Comparable<Item> {
        String tag;
        int count;

        private Item(String tag, int count) {
            this.tag = tag;
            this.count = count;
        }

        @Override
        public int compareTo(Item another) {
            if (this.count < another.count) return -1;
            if (this.count > another.count) return 1;
            return 0;
        }
    }

    private Item[] mItems = new Item[100];
    private int mSize;

    public TagMaxHeap() {

    }

    private int parent(int index) {
        return (index - 1) / 2;
    }

    private int left(int index) {
        return index * 2 + 1;
    }

    private int right(int index) {
        return index * 2 + 2;
    }

    private void grow() {
        mItems = Arrays.copyOf(mItems, mItems.length * 2);
    }

    private void swap(int from, int to) {
        Item tmp = mItems[from];
        mItems[from] = mItems[to];
        mItems[to] = tmp;
    }

    private void bubble(int index) {
        while (index > 0 &&
                mItems[index].compareTo(mItems[parent(index)]) > 0) {
            swap(index, parent(index));
            index = parent(index);
        }
    }

    private void sink(int index) {
        if (left(index) < mSize) {
            if (mItems[index].compareTo(mItems[left(index)]) < 0) {
                swap(index, left(index));
                sink(left(index));
            }
        }
        if (right(index) < mSize) {
            if (mItems[index].compareTo(mItems[right(index)]) < 0) {
                swap(index, right(index));
                sink(right(index));
            }
        }
    }

    public String peek() {
        if (mSize == 0) {
            return null;
        }
        return mItems[0].tag;
    }

    public String pop() {
        if (mSize == 0) {
            return null;
        }

        String popped = mItems[0].tag;

        mItems[0] = mItems[mSize - 1];
        mItems[--mSize] = null;

        if (mSize > 0) {
            sink(0);
        }

        return popped;
    }

    /**
     * Push the tag into the heap or update its listing. Roughly O(nlogn) worst case (if we match
     * a leaf node that bubbles to the top).
     *
     * @param tag The tag to add to the heap.
     */
    public void push(String tag) {
        for (int i = 0; i < mSize; i++) {
            if (mItems[i].tag.equals(tag)) {
                mItems[i].count += 1;
                bubble(i);
                return;
            }
        }

        mItems[mSize++] = new Item(tag, 1);

        if (mSize == mItems.length) {
            grow();
        }
    }

    public int size() {
        return mSize;
    }

    public static void main(String... args) {
        TagMaxHeap heap = new TagMaxHeap();
        Map<String, Integer> frequency = new HashMap<String, Integer>();

        Random generator = new Random();
        int heapEntries = 1000;
        String[] candidates = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m"};

        for (int i = 0; i < heapEntries; i++) {
            String entry = candidates[generator.nextInt(candidates.length)];
            if (frequency.containsKey(entry)) {
                frequency.put(entry, frequency.get(entry) + 1);
            } else {
                frequency.put(entry, 1);
            }
            heap.push(entry);
        }

        int last = Integer.MAX_VALUE;
        while (heap.size() > 0) {
            String entry = heap.pop();
            if (frequency.get(entry) > last) {
                System.out.println("Failed");
            }
            System.out.println(String.format("%s: %d", entry, frequency.get(entry)));
        }
    }
}
