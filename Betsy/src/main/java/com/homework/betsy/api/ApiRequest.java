package com.homework.betsy.api;

import android.util.Log;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * Represents an Etsy API request.
 */
public class ApiRequest {
    private static final String LOG_TAG               = "ApiRequest";

    private static final String LISTINGS_ACTIVE_URL   = "https://api.etsy.com/v2/listings/active?api_key=%s&includes=MainImage";
    private static final String LISTINGS_TRENDING_URL = "https://api.etsy.com/v2/listings/trending?api_key=%s&includes=MainImage";

    private static final String UTF_8                 = "UTF8";

    private static String mApiKey;
    private final String mKeyword;
    private final int mOffset;

    /**
     * Represents the complete URL for the API request; generated the first time the request is
     * made.
     */
    private String mRequestUrl;

    private ApiRequest(Builder builder) {
        mKeyword = builder.keyword;
        mOffset = builder.offset;
    }

    public static void setApiKey(String apiKey) {
        mApiKey = apiKey;
    }

    public String getRequestUrl() {
        if (mRequestUrl == null) {
            StringBuilder urlBuilder = new StringBuilder();
            urlBuilder.append(String.format(LISTINGS_ACTIVE_URL, mApiKey));
            if (mKeyword != null) {
                urlBuilder.append("&keywords=");
                try {
                    urlBuilder.append(URLEncoder.encode(mKeyword, UTF_8));
                } catch (UnsupportedEncodingException unexpected) {
                    urlBuilder.append(mKeyword);
                    Log.e(LOG_TAG, "We're not supporting UTF-8. That's cool.", unexpected);
                }
            }
            if (mOffset > 0) {
                urlBuilder.append("&offset=");
                urlBuilder.append(mOffset);
            }
            mRequestUrl = urlBuilder.toString();
        }
        return mRequestUrl;
    }

    public static class Builder {
        private int offset;
        private String keyword;

        public Builder setKeyword(String keyword) {
            this.keyword = keyword;
            return this;
        }

        public Builder setOffset(int offset) {
            this.offset = offset;
            return this;
        }

        public ApiRequest build() {
            return new ApiRequest(this);
        }
    }
}
