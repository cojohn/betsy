package com.homework.betsy.api;

import android.os.AsyncTask;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ApiTask extends AsyncTask<ApiRequest, Void, ApiResponse> {
    private static final String LOG_TAG = "BetsyApiTask";

    /**
     * Milliseconds before we've taken too long trying to connect to the API.
     */
    private static final int CONNECTION_TIMEOUT      = 10000;

    /**
     * Milliseconds before we've taken too long trying to get data.
     */
    private static final int SOCKET_TIMEOUT          = 30000;

    private static final String GET                  = "GET";

    public static enum ErrorCode {
        NETWORK_FAILURE,
        BAD_REQUEST,
        INVALID_URL
    }

    /**
     * The mResponseHandler
     */
    private ResponseListener mResponseListener;
    private ErrorCode mErrorCode;

    public ApiTask() {
        super();

        mResponseListener = ResponseListener.IGNORE_RESPONSE;
    }


    @Override
    protected ApiResponse doInBackground(ApiRequest... params) {
        if (params[0] == null) {
            return null;
        }

        URL url;
        try {
             url = new URL(params[0].getRequestUrl());
        } catch (MalformedURLException unexpected) {
            mErrorCode = ErrorCode.INVALID_URL;
            cancel(true);
            Log.e(LOG_TAG, "Invalid URL", unexpected);
            return null;
        }

        Log.v(LOG_TAG, String.format("Requesting API data from %s", params[0].getRequestUrl()));

        InputStream inputStream = null;
        HttpURLConnection connection = null;
        ListingsResponse listingsResponse = null;

        try {
            connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.setRequestMethod(GET);
            connection.setConnectTimeout(CONNECTION_TIMEOUT);
            connection.setReadTimeout(SOCKET_TIMEOUT);

            inputStream = connection.getInputStream();
            JsonReader jsonReader = new JsonReader(new InputStreamReader(inputStream, "UTF-8"));

            listingsResponse = new ListingsResponse();

            listingsResponse.fromJson(jsonReader);
            jsonReader.close();
        } catch (IOException unexpected) {
            Log.e(LOG_TAG, "Invalid something", unexpected);
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException reallyUnexpected) {
                    Log.e(LOG_TAG, "Failed to close InputStream", reallyUnexpected);
                }
            }
            if (connection != null) {
                connection.disconnect();
            }
        }

        return listingsResponse;
    }

    @Override
    protected void onPostExecute(ApiResponse response) {
        if (mResponseListener != null) {
            mResponseListener.onApiResponse(response);
        }
    }

    @Override
    public void onCancelled(ApiResponse response) {
        if (mResponseListener == null) {
            // Nothing is listening; so sad.
            return;
        }

        mResponseListener.onApiError(mErrorCode, response);
    }

    // Custom

    public void setResponseListener(ResponseListener responseListener) {
        mResponseListener = responseListener;
    }

    // Interfaces

    public interface ResponseListener {
        public static final ResponseListener IGNORE_RESPONSE = new ResponseListener() {
            @Override
            public void onApiResponse(ApiResponse response) {
                // La la la la la
            }

            @Override
            public void onApiError(ErrorCode errorCode, ApiResponse error) {
                // La la la la la
            }
        };

        public void onApiResponse(ApiResponse response);
        public void onApiError(ErrorCode errorCode, ApiResponse error);
    }
}
