package com.homework.betsy.api;

import android.util.JsonReader;
import android.util.Log;
import com.homework.betsy.TagMaxHeap;
import com.homework.betsy.model.Listing;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * An ApiResponse from an ApiRequest made to the active listings endpoint of the Etsy API.
 */
public class ListingsResponse extends ApiResponse {
    private static final String LOG_TAG              = "ListingsResponse";

    private static final String JSON_NAME_COUNT      = "count";
    private static final String JSON_NAME_RESULTS    = "results";
    private static final String JSON_NAME_PAGINATION = "pagination";

    private long mCount;
    private List<Listing> mListings;
    private TagMaxHeap mTagFrequency;

    public ListingsResponse() {
        mListings = new ArrayList<Listing>();
        mTagFrequency = new TagMaxHeap();
    }

    // Implementation for ApiResponse

    @Override
    public void fromJson(JsonReader reader) throws IOException {
        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();

            if (JSON_NAME_COUNT.equals(name)) {
                mCount = reader.nextLong();
                Log.d(LOG_TAG, String.format("%s = %d", name, mCount));
            } else if (JSON_NAME_RESULTS.equals(name)) {
                reader.beginArray();
                while (reader.hasNext()) {
                    // Parse all the listings.
                    Listing listing = Listing.fromJson(reader);
                    mListings.add(listing);
                    for (String tag : listing.getTags()) {
                        mTagFrequency.push(tag.toLowerCase());
                    }
                }
                reader.endArray();
            } else if (JSON_NAME_PAGINATION.equals(name)) {
                /*reader.beginObject();
                while (reader.hasNext()) {
                    Log.d(LOG_TAG, String.format("%s %d", reader.nextName(), reader.nextInt()));
                }
                reader.endObject();*/
                reader.skipValue();

            } else {
                reader.skipValue();
            }
        }

        reader.endObject();
    }

    // Custom

    public long getCount() {
        return mCount;
    }

    public List<Listing> getListings() {
        return mListings;
    }

    public TagMaxHeap getTags() {
        return mTagFrequency;
    }
}
