package com.homework.betsy.api;

import android.util.JsonReader;

import java.io.IOException;

/**
 * Structured response from the Etsy API.
 */
public abstract class ApiResponse {
    protected ApiResponse() {

    }

    public abstract void fromJson(JsonReader reader) throws IOException;
}
