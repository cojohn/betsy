package com.homework.betsy.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;

import java.io.IOException;

public class ImageData implements Parcelable {
    private static final String LOG_TAG                 = "ImageData";
    private static final String JSON_NAME_URL_75_X_75   = "url_75x75";
    private static final String JSON_NAME_URL_170_X_135 = "url_170x135";
    private static final String JSON_NAME_URL_570_X_N   = "url_570xN";
    private String mUrlString75x75;
    private String mUrlString170x135;
    private String mUrlString570xN;

    private ImageData(String smallUrlString, String medUrlString, String largeUrlString) {
        mUrlString75x75 = smallUrlString;
        mUrlString170x135 = medUrlString;
        mUrlString570xN = largeUrlString;
    }

    private ImageData(Parcel parcel) {
        mUrlString75x75 = parcel.readString();
        mUrlString170x135 = parcel.readString();
        mUrlString570xN = parcel.readString();
    }

    public String getUrl75x75() {
        return mUrlString75x75;
    }

    public String getUrl170x135() {
        return mUrlString170x135;
    }

    public String getUrl570xN() {
        return mUrlString570xN;
    }

    public static ImageData fromJson(JsonReader reader) throws IOException {
        String urlString75x75 = null;
        String urlString170x135 = null;
        String urlString570xN = null;

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();

            if (name.equals(JSON_NAME_URL_75_X_75)) {
                urlString75x75 = reader.nextString();
            } else if (name.equals(JSON_NAME_URL_170_X_135)) {
                urlString170x135 = reader.nextString();
            } else if (name.equals(JSON_NAME_URL_570_X_N)) {
                urlString570xN = reader.nextString();
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();

        return new ImageData(urlString75x75, urlString170x135, urlString570xN);
    }

    // Implementation for Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUrlString75x75);
        dest.writeString(mUrlString170x135);
        dest.writeString(mUrlString570xN);
    }

    public static final Parcelable.Creator<ImageData> CREATOR
            = new Parcelable.Creator<ImageData>() {
        public ImageData createFromParcel(Parcel in) {
            return new ImageData(in);
        }

        public ImageData[] newArray(int size) {
            return new ImageData[size];
        }
    };
}
