package com.homework.betsy.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

/**
 * An instance of a product listing returned from the Esty API.
 */
public class Listing implements Parcelable {
    private static final String LOG_TAG         = "Listing";

    private static final String JSON_NAME_DESC  = "description";
    private static final String JSON_NAME_ID    = "listing_id";
    private static final String JSON_NAME_TITLE = "title";
    private static final String JSON_NAME_IMAGE = "MainImage";
    private static final String JSON_NAME_TAGS  = "tags";

    private String mDescription;
    private long mId;
    private ArrayList<String> mTags;
    private String mTitle;
    private ImageData mImageData;

    private Listing(Builder builder) {
        mDescription = builder.description;
        mId = builder.id;
        mImageData = builder.imageData;
        mTags = builder.tags;
        mTitle = builder.title;
    }

    private Listing(Parcel parcel) {
        mDescription = parcel.readString();
        mId = parcel.readLong();
        mTitle = parcel.readString();
        mImageData = parcel.readParcelable(ImageData.class.getClassLoader());
        mTags = parcel.readArrayList(String.class.getClassLoader());
    }

    public static Listing fromJson(JsonReader reader) throws IOException {
        Builder builder = new Builder();

        reader.beginObject();

        while (reader.hasNext()) {
            String name = reader.nextName();

            if (name.equals(JSON_NAME_ID)) {
                builder.setId(reader.nextLong());
            } else if (name.equals(JSON_NAME_TITLE)) {
                builder.setTitle(reader.nextString());
            } else if (name.equals(JSON_NAME_IMAGE)) {
                builder.setImageData(ImageData.fromJson(reader));
            } else if (name.equals(JSON_NAME_DESC)) {
                builder.setDescription(reader.nextString());
            } else if (name.equals(JSON_NAME_TAGS)) {
                ArrayList<String> tags = new ArrayList<String>();
                reader.beginArray();
                while(reader.hasNext()) {
                    tags.add(reader.nextString());
                }
                reader.endArray();
                builder.setTags(tags);
            } else {
                reader.skipValue();
            }
        }

        reader.endObject();

        return builder.build();
    }

    public String getDescription() {
        return mDescription;
    }

    public long getId() {
        return mId;
    }

    public ImageData getImageData() {
        return mImageData;
    }

    public ArrayList<String> getTags() {
        return mTags;
    }

    public String getTitle() {
        return mTitle;
    }

    // Implementation for Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mDescription);
        dest.writeLong(mId);
        dest.writeString(mTitle);
        dest.writeParcelable(mImageData, flags);
        dest.writeStringList(mTags);
    }

    public static final Parcelable.Creator<Listing> CREATOR
            = new Parcelable.Creator<Listing>() {
        public Listing createFromParcel(Parcel in) {
            return new Listing(in);
        }

        public Listing[] newArray(int size) {
            return new Listing[size];
        }
    };

    // Builder; Listings can have lots of properties.

    public static final class Builder {
        private String description;
        private long id;
        private ImageData imageData;
        private ArrayList<String> tags;
        private String title;
        public void setDescription(String description) {
            this.description = description;
        }
        public void setId(long id) {
            this.id = id;
        }
        public void setImageData(ImageData imageData) {
            this.imageData = imageData;
        }
        public void setTags(ArrayList<String> tags) {
            this.tags = tags;
        }
        public void setTitle(String title) {
            this.title = title;
        }
        public Listing build() {
            return new Listing(this);
        }
    }
}
